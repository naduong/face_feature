import numpy as np
import cv2
import hog
import matplotlib.pyplot as plt


im_path = 'face-03_crop.jpg'
im = cv2.imread(im_path)
r = im[:, :, 0]
g = im[:, :, 1]
b = im[:, :, 2]


hog_r = hog.hog(r, cell_size=(2, 2), cells_per_block=(2, 2), visualise=False, nbins=9, signed_orientation=False, normalise=True)
hog_g = hog.hog(g, cell_size=(2, 2), cells_per_block=(2, 2), visualise=False, nbins=9, signed_orientation=False, normalise=True)
hog_b = hog.hog(b, cell_size=(2, 2), cells_per_block=(2, 2), visualise=False, nbins=9, signed_orientation=False, normalise=True)
print hog_r.shape
im2 = np.zeros(im.shape)
hog_r_v = hog.visualise_histogram(hog_r, 16, 16, False)
hog_g_v = hog.visualise_histogram(hog_g, 16, 16, False)
hog_b_v = hog.visualise_histogram(hog_b, 16, 16, False)
#
print hog_r_v.shape
# cv2.imshow('hist', im2)
# cv2.waitKey()
plt.figure()
plt.title('HOG features')
plt.imshow(im2, cmap=plt.cm.Greys_r)

plt.show()
