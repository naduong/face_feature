# README #


### What is this repository for? ###
A library to preprocess face images: extracting faces from an image (with or without alignment), create a bag of face crop parts, and extracting features from each crop.

Extracting faces from an image example (with alignment and without alignment)
![Scheme](img/alignment3.png)

Creating face crop bag example.
![Scheme](img/crop_bag1.png)

Under development for extracting features (hog, sift, lbp, edges)

### How do I get set up? ###
Dependencies: opencv, dlib, imutils (from pyimagesearch)

