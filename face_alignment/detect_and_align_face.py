import cv2
import os, imghdr
import argparse
import sys
sys.path.append('../basic')
import face_detector as fd
import crop_face as cf
import rotate_face as rf


STANDARD_HEIGHT = 500

## Initiate landmark predictor and reuse them as global objects
## detector is for detecting face
## predictor is for landmark prediction
detector, predictor = fd.initiateDetector()

## Distance between two eyes, reference for scaling image
EYE_DISTANCE = 100

CROP_PARTS = [0]

def getCropPartString(cropParts):
    name = '_bio_'
    for item in cropParts: name = name + str(item)   
    return name

CROP_PARTS_STRING = getCropPartString(CROP_PARTS)

def crop_and_align(image):

    # detect landmarks of all faces in the image
    landmarksOfAllFaces = fd.detectFaces(image, detector, predictor)

    croppedFaces = list()

    ## Use data of each face to crop and scale that face
    for faceIndex in range(0, len(landmarksOfAllFaces)):
        landmarks = landmarksOfAllFaces[faceIndex]

        ## scale image according to inter eye distance preference
        ## NOTE: this step is NOT OPTIMIZED. It could be done in better way.
        scaledImage, scaledLandmarks = cf.scaleFace(
            image,
            landmarks,
            EYE_DISTANCE)


        ## rotate face based on the reference angle between a line that
        ## connecting 2 eyes and x-axis
        rotatedFace, adjustedLandmarks = rf.rotateFace(scaledImage, scaledLandmarks)

        # croppedFace is a tuple containing crop face image and corresponding landmark set
        croppedFace = cf.crop_fixed_box(rotatedFace,adjustedLandmarks, EYE_DISTANCE)

        croppedFaces.append(croppedFace)

    return croppedFaces

        
def processImage(imageName, newFolderName, debugFolderPath, folderPath = os.getcwd()):
    ###############################################################################
    #
    # Function to read in image from image path, process image and save that image
    #
    # return code. 0 means failed, 1 means successful.
    #
    ###############################################################################

    ## read image from image name and folder path
    imagePath = os.path.join(folderPath, imageName)
    
    ## check if the imagePath is a image file or not
    imageType = imghdr.what(imagePath)
    if (imageType is None) or (imageType is 'gif'):
        print "\n[ERROR-processImage]: This file "+ imagePath + " is not an image"
        return

    ## Read image in as in gray scale
    image = cv2.imread(imagePath)        
    if image is None:
        print "[ERROR-processImage]: Cannot read image from path " + imagePath
        return 

    ## get fileName to produce a new file
    name_no_ext, extension = os.path.splitext(imageName)

    print "\n[INFO-processImage]: Processing " + os.path.join(folderPath, imageName) + "..."

    ## get all cropped faces in an image
    croppedFaces = crop_and_align(image)

    for index in range(len(croppedFaces)):
        fileName = name_no_ext + 'face_' +str(index) + extension

        # 'C0' is denoted for crop_part 0 - full face
        face, landmarks = croppedFaces[index]
        cv2.imwrite(os.path.join(debugFolderPath, fileName), face)


# # get argument in
# argParser = argparse.ArgumentParser()
# argParser.add_argument("-i", "--image", help = "path to input image")
# argParser.add_argument("-f", "--folder", help = "path to image folder")
# argParser.add_argument("-nf", "--newFolder", help = "result folder name")

# ag = argParser.parse_args()

# ## prepare new folder to save all detected cropped faces
# if ag.newFolder is not None:
#    newFolder = ag.newFolder
#    debugFolder  = newFolder + "_debug"
# else:
#     newFolder = "_detected"

# ## process a single image if argument is image type
# if ag.image is not None: 
#     path, fileName = os.path.split(ag.image)
#     processImage(fileName, newFolder, path)

# ## if the argument is a folder, process each file in a folder
# elif ag.folder is not None:
#     ## Dictionary to store all feature shape of of face    
#     featureListDict = dict()    
#     shapeList = list()
#     readJsonList = list()
    
#     ## read all sub folder to get and process images    
#     for subfolder in os.listdir(ag.folder):
#         subfolderPath = os.path.join(ag.folder, subfolder)
        
#         ## prepare new folder path to save face
#         newFolderPath = os.path.join(newFolder, subfolder)
#         if not os.path.exists(newFolderPath):
#             print "make new folder = " + newFolderPath
#             os.makedirs(newFolderPath)
            
#         ## if there is more than 1 face in an image, save that image to this folder
#         debugFolderPath = os.path.join(debugFolder, subfolder)
#         if not os.path.exists(debugFolderPath):
#             os.makedirs(debugFolderPath)

#         for imageName in os.listdir(subfolderPath):
#             processImage(imageName, newFolderPath, debugFolderPath,  subfolderPath)
# else:
#     print "No argument. Please try --help to see options."
