import numpy as np
import cv2
import detect_and_align_face as alignment

FULL_FACE = list((range(0, 26)))

LEFT_EYE = list(range(42, 48))
RIGHT_EYE = list(range(36, 42))
LEFT_BROW = list(range(22, 27))
RIGHT_BROW = list(range(17, 22))
NOSE = list(range(27, 35))
MOUTH = list(range(48, 61))

OVERLAY_POINTS = [ FULL_FACE ]
FEATHER_AMOUNT = 7


def draw_convex_hull(im, points, color):
    points = cv2.convexHull(points)
    cv2.fillConvexPoly(im, points, color=color)


def get_face_mask(im, landmarks):
    im = np.zeros(im.shape[:2], dtype=np.float64)

    for group in OVERLAY_POINTS:
        draw_convex_hull(im,
                         landmarks[group],
                         color=1)

    im = np.array([im, im, im]).transpose((1, 2, 0))

    im = (cv2.GaussianBlur(im, (FEATHER_AMOUNT, FEATHER_AMOUNT), 0) > 0) * 1.0
    im = cv2.GaussianBlur(im, (FEATHER_AMOUNT, FEATHER_AMOUNT), 0)

    return im

