import cv2
import numpy as np
from scipy.signal import convolve2d
import scipy


def get_kernel(a=0.4):
    """
    based this explanation to get robust 5x5 mask kernel
    http://crcv.ucf.edu/courses/CAP5415/Fall2012/Lecture-7-Pyramids.pdf
    :param a: a = 0.4 - approximate gaussian kernel
              a = 0.5 - triangular
    :return:
    """
    del_1d = np.array([[0.25 - a / 2.0, 0.25, a, 0.25, 0.25 - a / 2.0]]).T
    return del_1d.dot(del_1d.T)


def down_sampling(image):
    """
    down sampling by blurring image and reduce size to (h/2, w/2)
    """
    # get approximate 5x5 gaussian kernel and do convolution to blur image
    kernel = get_kernel()
    down_sample = scipy.signal.convolve2d(image, kernel, 'same')

    # omit every other row and column to reduce size
    return down_sample[::2, ::2]


def up_sampling(image):
    """
    Up sampling to size (2*h, 2*w) by repeating each row and column twice
    and do interpolation by approximate gaussian
    """
    # expand by repeating each row and column
    up_sample = np.zeros((image.shape[0] * 2, image.shape[1] * 2), dtype=np.float64)
    up_sample[::2, ::2] = image[:, :]

    # do interpolation by gaussian smoothing
    return scipy.signal.convolve2d(up_sample, get_kernel(), 'same')


def get_gaussian_pyramid(img, num_of_level):
    """
    create gaussian pyramid with provided number of level
    :param img: single chanel image to get pyramid
    :param num_of_level:
    :return: list of single images that form pyramid
    """
    if img is None:
        raise IOError('Input is not an image.')

    if num_of_level <= 0:
        raise ValueError('Level number needs to be greater than 0.')

    # add base level to pyramid
    img_c = img.copy()
    gau_py_img = [img_c.astype(float)]

    # down-sample image and add to pyramid
    for i in range(num_of_level):
        img_c = down_sampling(img_c)
        gau_py_img.append(img_c)

    return gau_py_img


def get_laplacian_pyramid(img, num_of_level):
    """
    approximate laplacian by difference of gaussian (DoG) and add to list
    :param img:
    :param num_of_level:
    :return: list of laplacian of image
    """

    # get gaussian pyramid
    py = get_gaussian_pyramid(img, num_of_level)

    # base level of laplacian pyramid
    lap = [py[len(py) - 1]]

    # calculate other level by DoG
    for i in range(len(py) - 1, 0, -1):
        g_e = up_sampling(py[i])
        # Note: make sure that the size of g_e and L are the same
        h, w = g_e.shape
        if h > py[i - 1].shape[0]: g_e = np.delete(g_e, (-1), axis=0)
        if w > py[i - 1].shape[1]: g_e = np.delete(g_e, (-1), axis=1)

        # get DoG
        L = cv2.subtract(py[i - 1].astype(float), g_e)
        lap.append(L)

    return lap


def visual_pyramid(pyr):
    """
    Function to visual pyramid
    """
    import matplotlib.pyplot as plt

    count = len(pyr)
    fig = plt.figure()
    for i in range(count):
        fig.add_subplot(1, count, i + 1)
        plt.imshow(pyr[i])

    plt.show()

def get_im_from_pyramid(pyr):
    """
    Construct image from laplacian pyramid by adding up all levels
    """

    blended = pyr[0]
    for i in range(1, len(pyr)):
        # up sample first
        blended = up_sampling(blended)

        # make sure up sampling image has the same dimension at next level
        h, w = blended.shape
        if h > pyr[i].shape[0] : blended = np.delete(blended, (-1), axis=0)
        if w > pyr[i].shape[1] : blended = np.delete(blended, (-1), axis=1)

        # adding
        blended = cv2.add(blended, pyr[i])

    return blended
