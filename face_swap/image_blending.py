import cv2
import numpy as np
import image_pyramid as pyramid

def resize_img(img, new_shape):
    """
    Function to resize image
    """
    img_shape = img.shape

    if new_shape[0] == img_shape[0]:
        return img

    # get scale ratio to find new dimension
    scale_ratio = float(new_shape[0]) / float(img_shape[0])

    # x_dim = num of cols, y_dim = num of rows
    new_dim = (int(scale_ratio * img_shape[1]), new_shape[0])

    return cv2.resize(img, new_dim, interpolation=cv2.INTER_AREA)


def blend_no_mask(im1, im2, numb_of_level=5):
    """
    blending no mask by combining image horizontally
    """
    # generate laplacian pyramid
    lap1 = pyramid.get_laplacian_pyramid(im1, numb_of_level)
    lap2 = pyramid.get_laplacian_pyramid(im2, numb_of_level)

    # Now add left and right halves of images in each level
    LS = []
    for lap_1, lap_2 in zip(lap1, lap2):
        rows, cols = lap_1.shape
        ls = np.hstack((lap_1[:, 0:cols / 2], lap_2[:, cols / 2:]))
        LS.append(ls)

    # construct blended image from laplacian pyramid
    return get_im_from_pyramid(LS)


def blend_with_mask(im1, im2, mask, numb_of_level=5):
    """
    Blend 2 images using mask
    """
    # generate laplacian pyramid for both images
    lap1 = pyramid.get_laplacian_pyramid(im1, numb_of_level)
    lap2 = pyramid.get_laplacian_pyramid(im2, numb_of_level)

    # generate gaussian image for mask
    G_m = pyramid.get_gaussian_pyramid(mask, numb_of_level)

    LS = list()
    for index in range(len(G_m)):
        # extract each level image from pyramid
        lap_1 = lap1[index]
        lap_2 = lap2[index]

        # mask have reverse pyramid compared to other two
        g_m = G_m[len(G_m) - 1 - index] / 255.

        # mask has the same dimension as blending images
        # h, w = g_m.shape
        # if h > lap_1.shape[0]: 
        #     for count in range(h - lap_1.shape[0]):
        #         g_m = np.delete(g_m, (-1), axis=0)
        #         g_m = np.delete(g_m, (-1), axis=1)
        # view = np.hstack((lap_1/255., lap_2/255.))
        # view = np.hstack((view, g_m))
        # cv2.imshow('view', view)
        # cv2.waitKey()
        # blending and appending to new pyramid
        LS.append(g_m * lap_2 + (1 - g_m) * lap_1)

    # construct from pyramid and return
    return get_im_from_pyramid(LS)


def get_im_from_pyramid(pyr):
    """
    contruct image from laplacian pyramid by adding up all levels
    """

    blended = pyr[0]
    for i in range(1, len(pyr)):
        # up sample first
        blended = pyramid.up_sampling(blended)

        # make sure up sampling image has the same dimension at next level
        h, w = blended.shape
        if h > pyr[i].shape[0]: blended = np.delete(blended, (-1), axis=0)
        if w > pyr[i].shape[1]: blended = np.delete(blended, (-1), axis=1)

        # adding
        blended = cv2.add(blended, pyr[i])

    return blended

def blend_RGB(im1, im2, mask = None):
    """
    Blending 2 RGB images
    """
    # make sure 2 input images has the same dimensions
    if im1.shape[0] > im2.shape[0]:
        im1 = resize_img(im1, im2.shape)
    else:
        im2 = resize_img(im2, im1.shape)

    # split channels from each image
    (b_1, g_1, r_1) = cv2.split(im1)
    (b_2, g_2, r_2) = cv2.split(im2)


    # do blending for each channels
    if mask is None:
        b = blend_no_mask(b_1, b_2)
        g = blend_no_mask(g_1, g_2)
        r = blend_no_mask(r_1, r_2)
    else:
        b = blend_with_mask(b_1, b_2, mask, numb_of_level = 7)
        g = blend_with_mask(g_1, g_2, mask, numb_of_level = 7)
        r = blend_with_mask(r_1, r_2, mask, numb_of_level = 7)

    # merge and return
    return cv2.merge((b,g,r))
