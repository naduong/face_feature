import numpy as np
import cv2
import sys
sys.path.append('../face_alignment')
import detect_and_align_face as alignment

# face components to get mask
FULL_FACE = list((range(0, 26)))
LEFT_EYE = list(range(42, 48))
RIGHT_EYE = list(range(36, 42))
LEFT_BROW = list(range(22, 27))
RIGHT_BROW = list(range(17, 22))
NOSE = list(range(27, 35))
MOUTH = list(range(48, 61))

OVERLAY_POINTS = [ FULL_FACE ]
FEATHER_AMOUNT = 7

def draw_convex_hull(im, points, color):
    points = cv2.convexHull(points)
    cv2.fillConvexPoly(im, points, color=color)

def get_face_mask(im, landmarks):
    im = np.zeros(im.shape[:2], dtype=np.uint8)

    for group in OVERLAY_POINTS:
        draw_convex_hull(im,
                         landmarks[group],
                         color=1)

    im = np.array([im, im, im]).transpose((1, 2, 0))

    im = (cv2.GaussianBlur(im, (FEATHER_AMOUNT, FEATHER_AMOUNT), 0) > 0) * 1.0
    im = cv2.GaussianBlur(im, (FEATHER_AMOUNT, FEATHER_AMOUNT), 0)

    return im

def get_square_im(im):
    h, w = im.shape[0], im.shape[1]

    if h == w: return im

    if h > w:
        return cv2.resize(im [h - w : h, :, :], (h, h), cv2.INTER_LINEAR)
    else:
        return cv2.resize(im [: , w - h : w, :], (w, w), cv2.INTER_LINEAR)

#
# im_path = 'beckham.jpg'
# im = cv2.imread(im_path)
#
# face_set = alignment.crop_and_align(im)
# face, landmarks = face_set[0]
#
# mask = get_face_mask(face, landmarks)
# face = get_square_im(face)
# mask = get_square_im(mask)
# # cv2.imshow('mask', mask)
# # cv2.waitKey()
# cv2.imwrite('beckham_crop.jpg', face)
# cv2.imwrite('beckham_mask.jpg', mask * 255)
