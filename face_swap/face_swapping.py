import sys
sys.path.append('../face_alignment')
import detect_and_align_face as alignment
import face_mask
import cv2
import numpy as np
import image_blending as blender

#https://matthewearl.github.io/2015/07/28/switching-eds-with-python/

def swap_from_single_image():
    return None

def swap_from_two_images(image1, image2):
    face_set1 = get_faces_and_masks(image1)
    face_set2 = get_faces_and_masks(image2)

    face_1, mask_1 = face_set1[0]
    face_2, mask_2 = face_set2[0]
    cv2.imwrite('face_1.jpg', face_1)
    cv2.imwrite('face_2.jpg', face_2)
    cv2.imwrite('mask_1.jpg', mask_1)
    cv2.imwrite('mask_2.jpg', mask_2)

    view = np.hstack((face_1, face_2))
    mask = np.hstack((mask_1, mask_2))
    # view = np.vstack((view, mask))
    cv2.imshow('view', mask)
    cv2.waitKey()

    swap1 = blender.blend_RGB(face_1, face_2, mask_1)
    swap2 = blender.blend_RGB(face_2, face_1, mask_2)

    return (swap1, swap2)

def get_faces_and_masks(image):
    face_set = alignment.crop_and_align(image)

    faces_and_masks = []
    for face, landmarks in face_set:
        mask = face_mask.get_face_mask(face, landmarks)
        face = face_mask.get_square_im(face)
        mask = face_mask.get_square_im(mask)
        
        mask = cv2.cvtColor(np.uint8(mask), cv2.COLOR_BGR2GRAY)

        faces_and_masks.append((face, mask * 255))

    return faces_and_masks

im1_path = 'beckham.jpg'
im2_path = 'giang_2.jpg'

im1 = cv2.imread(im1_path)
im2 = cv2.imread(im2_path)

s1, s2 = swap_from_two_images(im1, im2)
cv2.imwrite('swap1.jpg', s1)
cv2.imwrite('swap2.jpg', s2)
