import cv2
import gc
import os, imghdr
import argparse
import sys
sys.path.append('../basic')
from basic import face_detector as fd
from basic import crop_face as cf
from basic import rotate_face as rf
from basic import eye_utils as eyes
from basic import crop_parts as cp


STANDARD_HEIGHT = 500

## Initiate landmark predictor and reuse them as global objects
## detector is for detecting face
## predictor is for landmark prediction
detector, predictor = fd.initiateDetector()

## Distance between two eyes, reference for scaling image
EYE_DISTANCE = 50

CROP_PARTS = [0,1,2,3,4,5,6,7,8,9,10,11]

def getCropPartString(cropParts):
    name = '_bio_'
    for item in cropParts: name = name + str(item)   
    return name

CROP_PARTS_STRING = getCropPartString(CROP_PARTS)

def extract_face(image, scaling = False, aligned = False, getCropped = False):
    # detect landmarks of all faces in the image
    landmarksOfAllFaces = fd.detectFaces(image, detector, predictor)

    croppedFaces = list()

    ## Use data of each face to crop and scale that face
    for faceIndex in range(0, len(landmarksOfAllFaces)):
        print (faceIndex)
        landmarks = landmarksOfAllFaces[faceIndex]

        distance = int(eyes.getDistance(landmarks))

        if scaling is True:
            ## scale image according to inter eye distance preference
            ## NOTE: this step is NOT OPTIMIZED. It could be done in better way.
            ## Likely, this step with cause error with the image size too large
            ## here is a quick fix
            ## https://stackoverflow.com/questions/31996367/opencv-resize-fails-on-large-image-with-error-215-ssize-area-0-in-funct
            image, landmarks = cf.scaleFace(image, landmarks, EYE_DISTANCE)
            distance = EYE_DISTANCE

        if aligned is True:
            ## rotate face based on the reference angle between a line that
            ## connecting 2 eyes and x-axis
            n_image, n_landmarks = rf.rotateFace(image, landmarks)
        else:
            n_image = image
            n_landmarks = landmarks

        if getCropped is True:
            # crop a face into 12 different parts
            croppedFace, aLandmarks = cf.cropFace(n_image, n_landmarks, 0)
            cropParts = cp.crop(croppedFace, aLandmarks, CROP_PARTS)
            croppedFaces.append(cropParts)
        else:
            # croppedFace is a tuple containing crop face image and corresponding landmark set
            croppedFace = cf.crop_fixed_box(n_image, n_landmarks, distance)

    return croppedFaces

        
def processImage(imageName, newFolderName, debugFolderPath, folderPath = os.getcwd()):
    ###############################################################################
    #
    # Function to read in image from image path, process image and save that image
    #
    # return code. 0 means failed, 1 means successful.
    #
    ###############################################################################

    ## read image from image name and folder path
    imagePath = os.path.join(folderPath, imageName)
    
    ## check if the imagePath is a image file or not
    imageType = imghdr.what(imagePath)
    if (imageType is None) or (imageType is 'gif'):
        print ("\n[ERROR-processImage]: This file ", imagePath , " is not an image")
        return

    ## Read image in as in gray scale
    image = cv2.imread(imagePath)        
    if image is None:
        print ("[ERROR-processImage]: Cannot read image from path " , imagePath)
        return 

    ## get fileName to produce a new file
    name_no_ext, extension = os.path.splitext(imageName)

    print ("\n[INFO-processImage]: Processing " , os.path.join(folderPath, imageName) , "...")

    ## get all cropped faces in an image
    getCropped = True
    croppedFaces = extract_face(image, scaling = False, aligned = True, getCropped = getCropped)

    for index in range(len(croppedFaces)):
        if getCropped is True:
            cropParts = croppedFaces[index]

            for cropName, crop in cropParts.items():
                newFileName = name_no_ext + "_" + str(index) + "_" + cropName + extension
                cv2.imwrite(os.path.join(debugFolderPath, newFileName), crop)    
        else:
            fileName = name_no_ext + 'face_' +str(index) + extension
            # 'C0' is denoted for crop_part 0 - full face
            face, landmarks = croppedFaces[index]
            cv2.imwrite(os.path.join(debugFolderPath, fileName), face)


# get argument in
argParser = argparse.ArgumentParser()
argParser.add_argument("-i", "--image", help = "path to input image")
argParser.add_argument("-f", "--folder", help = "path to image folder")
argParser.add_argument("-nf", "--newFolder", help = "result folder name")

ag = argParser.parse_args()

## prepare new folder to save all detected cropped faces
if ag.newFolder is not None:
   newFolder = ag.newFolder
   debugFolder  = newFolder + "_debug"
else:
    newFolder = "_detected"

## process a single image if argument is image type
if ag.image is not None: 
    path, fileName = os.path.split(ag.image)
    processImage(fileName, newFolder, path)

## if the argument is a folder, process each file in a folder
elif ag.folder is not None:
    ## Dictionary to store all feature shape of of face    
    featureListDict = dict()    
    shapeList = list()
    readJsonList = list()
    
    ## read all sub folder to get and process images    
    for subfolder in os.listdir(ag.folder):
        subfolderPath = os.path.join(ag.folder, subfolder)
        
        ## prepare new folder path to save face
        newFolderPath = os.path.join(newFolder, subfolder)
        if not os.path.exists(newFolderPath):
            print ("make new folder = " + newFolderPath)
            os.makedirs(newFolderPath)
            
        ## if there is more than 1 face in an image, save that image to this folder
        debugFolderPath = os.path.join(debugFolder, subfolder)
        if not os.path.exists(debugFolderPath):
            os.makedirs(debugFolderPath)

        for imageName in os.listdir(subfolderPath):
            processImage(imageName, newFolderPath, debugFolderPath,  subfolderPath)
else:
    print ("No argument. Please try --help to see options.")
