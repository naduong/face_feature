from imutils import face_utils
import dlib
import cv2
import face_box as fb

def initiateDetector():
    landmarkPredictorPath = "face/detectionModel/shape_predictor_68_face_landmarks.dat"
    return dlib.get_frontal_face_detector(), dlib.shape_predictor(landmarkPredictorPath)


def detectFaces(grayImage, detector, predictor):
    ###########################################################################
    #
    # Note: How to handle the case when an image has multiple faces?????
    #
    ###########################################################################
    #grayImage = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # detect faces using HOG features - dlib
    dlibRects = detector(grayImage, 1)

    landmarksOfAllFaces = list()

    for (i, rect) in enumerate(dlibRects):
        ## predict landmarks using dlib predictor
        shape = predictor(grayImage, rect)

        ## convert to numpy array
        landmarks = face_utils.shape_to_np(shape)
      
        landmarksOfAllFaces.append(landmarks)
        
    if len(landmarksOfAllFaces) > 1:
        landmarksOfAllFaces = eleminateBadFace(landmarksOfAllFaces)

    return landmarksOfAllFaces


def detectAndDraw(image, detector, predictor):
    ###########################################################################
    #
    # Function to predict face landmarks using DLib
    #
    # Return an image with face and landmarks detected
    #
    ###########################################################################

    landmarksOfAllFaces = detectFaces(image, detector, predictor)
    
    if len(landmarksOfAllFaces) > 1:
        landmarksOfAllFaces = eleminateBadFace(landmarksOfAllFaces)

    for faceIndex in range (0, len(landmarksOfAllFaces)):
        faceBox = fb.getFaceBox(landmarksOfAllFaces[faceIndex], 1)
        
        cv2.rectangle(image,
            (faceBox[0], faceBox[1]),
            (faceBox[0] + faceBox[2], faceBox[1] + faceBox[3]),
            (255, 0, 0),
            2)

        landmarks = landmarksOfAllFaces[faceIndex]

        for index in range (0, len(landmarks)):
            pos = landmarks[index]
            cv2.circle(image, (pos[0], pos[1]), 2, (0, 0, 255), 1)

    return image


def eleminateBadFace(landmarksOfAllFaces):
    ###########################################################################
    #
    # Function to get only quality face for matching
    #
    ###########################################################################
    faceBoxList = []
    
    for faceIndex in range (0, len(landmarksOfAllFaces)):
        faceBoxList.append(fb.getFaceBox(landmarksOfAllFaces[faceIndex], 1))
         
    maxHeight = max([faceBox[3] for faceBox in faceBoxList])
    
    landmarksOfGoodFaces = []
    for faceIndex in range(0, len(landmarksOfAllFaces)):
        if faceBoxList[faceIndex][3] > maxHeight / 1.5:
            landmarksOfGoodFaces.append(landmarksOfAllFaces[faceIndex])
    
    return landmarksOfGoodFaces

def draw(image, landmarks):
    ###########################################################################
    #
    #
    ###########################################################################

    assert image is not None, "[displayLandmarksByData]: image is NONE"
    assert landmarks is not None, "[displayLandmarksByData]: landmarks are NONE"
    display_image = image.copy()
    for index in range (0, len(landmarks)):
        pos = landmarks[index]
        cv2.circle(display_image, (pos[0], pos[1]), 2, (0, 0, 255), 1)

    return display_image
