import numpy as np
import math
import landmarks_utils as lu

def getAngle(shape):
    ###########################################################################
    #
    # get the angle between the line connecting two eyes and x-axis
    #
    ###########################################################################
    assert shape is not None, "cannot getMainLandmarks..."

    eyes = lu.getEyes(shape)

    basePointX = eyes[1][0]  ## rightEyeX
    basePointY = eyes[0][1]  ## leftEyeY

    eyeVectorX = eyes[1][0] - eyes[0][0]   ## rightEyeX - leftEyeX
    eyeVectorY = eyes[1][1] - eyes[0][1]   ## rightEyeY - leftEyeY

    baseVectorX = basePointX - eyes[0][0]  ## basePointX - leftEyeX
    baseVectorY = basePointY - eyes[0][1]  ## basePointY - leftEyeY

    innerProduct = eyeVectorX * baseVectorX + eyeVectorY * baseVectorY
    eyeVectorLen = math.hypot(eyeVectorX, eyeVectorY)
    baseVectorLen = math.hypot(baseVectorX, baseVectorY)

    angle = math.acos(innerProduct / (eyeVectorLen * baseVectorLen)) * 180 / math.pi

    if eyes[1][1] > eyes[0][1]:     ## rightEyeY > leftEyeY:
        return -angle

    return angle

def getDistance(shape):
    ###########################################################################
    #
    # Calculate distance between center of left eye and center of right eye
    #
    ###########################################################################
    assert shape is not None, "cannot getMainLandmarks..."

    landmarks = list()

    eyes = lu.getEyes(shape)

    ##     math.sqrt((rightEyeX - leftEyeX)**2 + (rightEyeY - leftEyeY)**2)
    return math.sqrt((eyes[1][0] - eyes[0][0])**2 + (eyes[1][1] - eyes[0][1])**2)
