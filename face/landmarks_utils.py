import numpy as np

leftBoundIndicator = [17, 0, 1, 2, 3]
rightBoundIndicator = [26, 16, 15, 14, 13]
upperBoundIndicator = [18, 19, 20, 23, 24, 25]
lowerBoundIndicator = [7, 8, 9]
leftEyeIndicator = [36, 37, 38, 39, 40, 41]
rightEyeIndicator = [42, 43, 44, 45, 46, 47]
upperMouthIndicator = [50, 51, 52]
lowerMouthIndicator = [48, 54, 55, 56, 57, 58, 59]

def getMainLandmarks(shape):
    ###########################################################################
    #
    # Main landmarks in a face include:
    #     0 -- left eye
    #     1 -- right eye
    #     2 -- nose
    #     3 -- upper mouth
    #     4 -- bottom mouth
    #     5 -- left mouth
    #     6 -- right mouth
    #
    ###########################################################################
    assert shape is not None, "[landmarks_utils--getMainLandmarks]: shape is NONE."

    landmarks = list()

    eyes = getEyes(shape)

    landmarks.append(eyes[0])                                     ## left eye
    landmarks.append(eyes[1])                                     ## right eye
    landmarks.append((shape[33][0], shape[33][1]))                ## bottom nose
    landmarks.append(getCoordinateOf(shape, upperMouthIndicator)) ## upper mouth
    landmarks.append(getCoordinateOf(shape, lowerMouthIndicator)) ## lower mouth
    landmarks.append((shape[48][0], shape[48][1]))                ## left mouth
    landmarks.append((shape[54][0], shape[54][1]))                ## right mouth

    return landmarks

def getCoordinateOf(shape, landmarkIndicator):
    ###########################################################################
    #
    # Helper function to get positions of main landmarks
    #
    ###########################################################################
    assert shape is not None, "[landmarks_utils--getCoordinateOf]: shape is NONE."
    assert landmarkIndicator is not None, "[landmarks_utils--getCoordinateOf]: landmarkIndicator is NONE."

    X_coord = int(np.mean([shape[index][0] for index in landmarkIndicator]))
    Y_coord = int(np.mean([shape[index][1] for index in landmarkIndicator]))

    return (X_coord, Y_coord)


def getEyes(shape):
    ###########################################################################
    #
    # Function to get center positions of left eye and right eye
    #
    ###########################################################################

    leftEye_coord = getCoordinateOf(shape, leftEyeIndicator)
    rightEye_coord = getCoordinateOf(shape, rightEyeIndicator)

    return (leftEye_coord, rightEye_coord)

