bigBoundingBox = (0.15, 0.15, 0.3, 0.15)
smallBoundingBox = (0.07, 0.07, 0.2, 0.1)
preCropBoundingBox = (1, 1, 1, 1)
leftBoundIndicator = [17, 0, 1, 2, 3]
rightBoundIndicator = [26, 16, 15, 14, 13]
upperBoundIndicator = [18, 19, 20, 23, 24, 25]
lowerBoundIndicator = [7, 8, 9]

def getFaceBox(shape, boundingBoxType):
    ###########################################################################
    #
    # This face box will be used to crop a face
    #
    ###########################################################################

    assert shape is not None, "cannot getMainLandmarks..."

    # Calculate most left point and most right point of a face
    minX = min([shape[index][0] for index in leftBoundIndicator])
    maxX = max([shape[index][0] for index in rightBoundIndicator])
    minY = min([shape[index][1] for index in upperBoundIndicator])
    maxY = max([shape[index][1] for index in lowerBoundIndicator])

    ## get boundingBoxType by passed-in flag
    if boundingBoxType == 0:
        boundingBox = smallBoundingBox
    elif boundingBoxType == 1:
        boundingBox = preCropBoundingBox
    else:
        boundingBox = bigBoundingBox

    leftBoundVal = max(int(minX - (maxX - minX) * boundingBox[0]), 0)
    rightBoundVal = int(maxX + (maxX - minX) * boundingBox[1])
    upperBoundVal = max(int(minY - (maxY - minY) * boundingBox[2]), 0)
    lowerBoundVal = int(maxY + (maxY - minY) * boundingBox[3])

    return (leftBoundVal, upperBoundVal, (rightBoundVal - leftBoundVal), (lowerBoundVal - upperBoundVal))
