###############################################################################
#
# High level procedure:
#     1. Detect faces and landmarks in the original image
#     2. crop faces from the original image
#     3. Scale faces to the same same reference (Inter eyes = 40 or ...)
#     4. Scale position of landmarks accordingly
#     5. Create faces into 8 different samples:
#         - Full face           (FF  -  0)
#         - Left face           (LF  -  1)
#         - Right face          (RF  -  2)
#         - Left Eye and Nose   (LEN -  3)
#         - Right Eye and Nose  (REN -  4)
#         - Two Eyes            (TE  -  5)
#         - Two Eyes and Nose   (TEN -  6)
#         - Nose and Mouth      (NM  -  7)
#
#
###############################################################################

import cv2
from . import face_detector as fd
from . import eye_utils as eu
from . import face_box as fb
import numpy as np
from . import landmarks_utils as lu


def scaleFace(image, landmarks, eyesDistance):
    ###########################################################################
    #
    #
    ###########################################################################
    assert image is not None, "[scaleFace]: image is NONE"
    assert landmarks is not None, "[scaleFace]: landmarks is NONE"
    assert eyesDistance > 0, "[scaleFace]: eyesDistance is invalid. Need positive value"

    ## get the eye distance from the image and compute the scale ratio
    originalEyesDistance = eu.getDistance(landmarks)
    scaledRatio = eyesDistance / originalEyesDistance

    height, width = image.shape[0], image.shape[1]

    ## scale the image and landmarks accordingly to the scale ratio
    scaledImage = cv2.resize(image, (int(width * scaledRatio), int(height * scaledRatio)),
        interpolation = cv2.INTER_AREA)
    scaledLandmarks = (np.matrix(landmarks) * scaledRatio).getA()

    return scaledImage, scaledLandmarks


def translateLandmarks(landmarks, X_Translation, Y_Translation):
    ###########################################################################
    #
    #
    ###########################################################################
    assert landmarks is not None, "[translateLandmarks]: landmarks are none"

    rows, cols = landmarks.shape[0:2]

    ## create translation matrix to shift landmarks
    landmarksTranslationMatrix = np.arange(rows * cols)
    landmarksTranslationMatrix.shape = (rows, cols)
    landmarksTranslationMatrix[:, 0] = X_Translation
    landmarksTranslationMatrix[:, 1] = Y_Translation

    ## convert landmarks arrays to matrix and translate by adding with
    ## translation matrix
    landmarksMatrix = np.matrix(landmarks)
    translatedLandmarks = landmarksMatrix + landmarksTranslationMatrix

    return translatedLandmarks.getA()


def crop_fixed_box(image, landmarks, eyeDistance):
    '''
    NOTE: In general case, the dimension after crop_fixed_box has a square
          dimension; however, for some cases, like face locates at the edge of
          image, the crop is not a square.
    :param image:
    :param landmarks:
    :param eyeDistance:
    :return:
    '''
    assert image is not None, "[CropFace]: image is NONE"
    assert landmarks is not None, "[CropFace]: landmarks is NONE"

    ## compute the list of main landmarks:
    #     0 -- left eye
    #     1 -- right eye
    #     2 -- nose
    #     3 -- upper mouth
    #     4 -- bottom mouth
    #     5 -- left mouth
    #     6 -- right mouth
    leftEye, rightEye = lu.getEyes(landmarks)

    widthOffset = int(eyeDistance)
    topOffset = int(eyeDistance)

    # find the coordinates of top left points and bottom right points for
    # cropping face
    startX = max(int(leftEye[0] - widthOffset), 0)
    startY = max(int(min(leftEye[1], rightEye[1]) - topOffset), 0)
    endX = min(startX + eyeDistance * 3, image.shape[1])
    endY = min(startY + eyeDistance * 3, image.shape[0])

    ## crop face using the coordinates found above
    translatedImage = image[startY : endY, startX : endX]

    ## using this value to shift coordinates of landmarks
    X_Translation = -(startX)
    Y_Translation = -(startY)

    rows, cols = image.shape[0], image.shape[1]

    ## create translation matrix to shift landmarks
    translatedLandmarks = translateLandmarks(landmarks, X_Translation, Y_Translation)


    return (translatedImage, translatedLandmarks.astype(int))


def cropFace(image, landmarks, boundingBoxType = 0):
    '''
    Return a tuple of cropped face image and corresponding landmark set
    '''

    assert image is not None, "[CropFace]: image is NONE"
    assert landmarks is not None, "[CropFace]: landmarks is NONE"


    ## get faceData from a face in an image. Face data includes:
    ##    0 -- X-value of most left point
    ##    1 -- Y-value of most left point
    ##    2 -- face width
    ##    3 -- face height
    faceData = fb.getFaceBox(landmarks, boundingBoxType)

    X_Translation = -faceData[0]
    Y_Translation = -faceData[1]

    rows, cols = image.shape[0], image.shape[1]

    translatedImage = image[faceData[1] : (faceData[1] + faceData[3]),
        faceData[0] : faceData[0] + faceData[2]]

    translatedLandmarks = translateLandmarks(landmarks, X_Translation, Y_Translation)

    return (translatedImage, translatedLandmarks.astype(int))
    

def cropByBoundingBox(image, boundingBox):
    ###########################################################################
    #
    #
    ###########################################################################
    assert image is not None, "[CropFace]: image is NONE"
    assert boundingBox is not None, "[CropFace]: boundingBox is NONE"

    return image[boundingBox[1] : (boundingBox[1] + boundingBox[3]),
        boundingBox[0] : boundingBox[0] + boundingBox[2]]