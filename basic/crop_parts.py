import numpy as np

## crop parts code
##   Code       Crop Parts
##   -------------------------------
##     0       Full Face
##     1       Two Eyes with Nose
##     2       Two Eyes
##     3       Left Face
##     4       Right Face
##     5       Left Eye with Nose
##     6       Right Eye with Nose
##     7       Left Eye
##     8       Right Eye
##     9       Nose
##    10       Nose and Mouth
##    11       Mouth

cropPartPoints = ([],                         ## empty for full face
    [17, 19, 20, 23, 24, 26, 33],             ## Two Eyes with Nose
    [17, 19, 20, 40, 41, 23, 24, 26, 46, 47], ## Two Eyes
    [],                                       ## empty for Left Face
    [],                                       ## empty fot Right Face
    [23, 24, 26, 33, 31],                     ## Left Eye With Nose
    [17, 19, 20, 35, 33],                     ## Right Eye with Nose
    [22, 23, 24, 26, 46, 47],                 ## left Eye
    [17, 19, 20, 21, 40, 41],                 ## right Eye
    [27, 31, 32, 33, 34, 35],                 ## Nose
    [28, 31, 35, 54, 56, 57, 58, 60],         ## Nose and Mouth
    [48, 49, 50, 51, 52, 53, 54, 56, 57, 58, 60]) ## Mouth

cropPartNames = ('C0','C1','C2','C3','C4','C5',
    'C6','C7','C8','C9','C10','C11')

def crop(croppedImage, shape, cropPartIndices):
    ###########################################################################
    ##
    ##
    ##
    ###########################################################################

    ## TODO: add error handling for croppedImage and cropPartCode

    cropParts = dict()

    for cropPartIndex in cropPartIndices:
        if cropPartIndex == 0:
            topLeftX, topLeftY, bottomRightX, bottomRightY = fullFaceCoord(croppedImage)
        elif cropPartIndex == 3:
            topLeftX, topLeftY, bottomRightX, bottomRightY = leftFaceCoord(croppedImage, shape)
        elif cropPartIndex == 4:
            topLeftX, topLeftY, bottomRightX, bottomRightY = rightFaceCoord(croppedImage, shape)
        else:
            topLeftX, topLeftY, bottomRightX, bottomRightY = cropPartCoord(croppedImage, shape, cropPartIndex)

        cropParts[cropPartNames[cropPartIndex]] = croppedImage[topLeftY : bottomRightY, topLeftX : bottomRightX]

    return cropParts


def fullFaceCoord(image):
    return 0, 0, image.shape[1], image.shape[0]

def leftFaceCoord(image, shape):
    return shape[31][0], 0, image.shape[1], image.shape[0]

def rightFaceCoord(image, shape):
    return 0, 0, shape[35][0], image.shape[0]

def cropPartCoord(image, shape, cropPartIndex):
    height, width = image.shape[0], image.shape[1]

    if cropPartIndex in {2, 7, 8, 9, 11}:
        offset = int (height / 10)
    else:
        offset = int(height / 80)

    topLeftX = max(0, min(shape[index][0] for index in cropPartPoints[cropPartIndex]) - offset)
    topLeftY = max(0, min(shape[index][1] for index in cropPartPoints[cropPartIndex]) - offset)

    bottomRightX = min(width, max(shape[index][0] for index in cropPartPoints[cropPartIndex]) + offset)
    bottomRightY = min(height, max(shape[index][1] for index in cropPartPoints[cropPartIndex]) + offset)

    if cropPartIndex in {1, 2, 6, 10, 11}: topLeftX = 0
    if cropPartIndex in {1, 2, 5, 10, 11}: bottomRightX = image.shape[1]
    if cropPartIndex in {10, 11}: bottomRightY = image.shape[0]

    return topLeftX, topLeftY, bottomRightX, bottomRightY


