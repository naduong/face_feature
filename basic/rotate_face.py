import cv2
import numpy as np
import math
from . import  face_detector as fd
from . import eye_utils as eu

def compare(oImage, olandmarks, rImage, rLandmarks):
    ###########################################################################
    #
    # put original image (oImage) and rotated image (rImage) together for
    # comparison
    #
    ###########################################################################

    oImage = fd.draw(oImage, olandmarks)
    rImage = fd.draw(rImage, rLandmarks)
    vis = np.concatenate((oImage, rImage), axis = 1)

    return vis

def rotateFace(image, landmarks):
    ###########################################################################
    #
    # rotate an image by the angle between a line connecting eyes and x-axis
    #
    ###########################################################################

    ## angle between a line connecting two eyes and x-axis
    angle = -eu.getAngle(landmarks)

    rows, cols = image.shape[0], image.shape[1]

    ## matrix to do image rotation
    ## the rotation is operated at center of the image (cols / 2, rows / 2)
    rotationMatrix = cv2.getRotationMatrix2D((cols / 2, rows / 2), angle, 1)

    ## affine operation to transform image using rotation matrix
    dist = cv2.warpAffine(image, rotationMatrix, (cols, rows))

    ## prepare landmarks 2D-arrays to get the corresponding coordinates after
    ## transformation
    ones = np.ones(shape=(len(landmarks),1))
    points_ones = np.hstack([landmarks, ones])

    ## transform landmark 2D array using dot product with rotation matrix
    transformedLandmarks = rotationMatrix.dot(points_ones.T).T

    return dist, transformedLandmarks
